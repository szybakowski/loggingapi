package com.example.demo.Controller;

import com.example.demo.Config.ApiProperties;
import com.example.demo.Model.User;
import com.example.demo.Repository.UserRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.Date;

@RestController
public class LoggingController extends ApiProperties {

    @Autowired
    PasswordEncoder passEncoder;

    @Autowired
    UserRepository userRep;


//to do zwaracj obiekt
    @PostMapping("/login")
    public String login(@RequestBody User user) {
        Long now = System.currentTimeMillis();
        User userSearched = userRep.findByLogin(user.getLogin());

        if (userSearched != null) {
            if (passEncoder.matches(user.getPassword(), userSearched.getPassword())) {
                return Jwts.builder()
                        .setSubject(user.getLogin())
                        .claim("roles", "user")
                        .setIssuedAt(new Date(now))
                        .setExpiration(new Date(now + 20000))
                        .signWith(SignatureAlgorithm.HS256, getSecretEncodeKey())
                        .compact();
            }
        }

        return "Bad login/password data";
    }

}